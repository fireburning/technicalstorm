package com.gionee.tutorial.javainsight1;

public class Inheritance {
    public static void main(String[] args) {
        DerivedClass superCls = new DerivedClass();
    }
}

class SuperClass {
    {
        System.out.println("super instance block");
    }
    static {
        System.out.println("super static block");
    }

    public SuperClass() {
        System.out.println("super instance");
        method();
    }

    public void method() {
        System.out.println("super instance method");
    }
}

class DerivedClass extends SuperClass {
    final int mField = 5;
    {
        System.out.println("Derived instance block mField="+mField);
    }
    static {
        System.out.println("Derived static block");
    }

    public DerivedClass() {
        System.out.println("Derived instance");
    }

    public void method() {
        System.out.println("Derived instance method mField="+mField);
    }
}
