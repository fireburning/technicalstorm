package com.gionee.tutorial.innerclass;
/*
 * Author:Pengfei.Ji
 */

public class InnerClass {
    public static interface StateChanger {
        public int change();
    }
    
    private int mState = 1;
    private static StateChanger mCache = null; 
    
    public StateChanger getStateChanger() {
        if (mCache == null) {
            mCache = new StateChanger() {
                public int change() {
                    return ++mState;
                }
            };
        }
        return mCache;
    }
    
    public int getState() {
        return mState;
    }
    
    public static void main(String[] args) {
        test();
        test();
        test();
    }
    
    private static void test() {
        InnerClass aObj = new InnerClass();
        StateChanger changer = aObj.getStateChanger();
        int rst = changer.change();
        System.out.println(rst);
        System.out.println(aObj.getState());
    }
}
