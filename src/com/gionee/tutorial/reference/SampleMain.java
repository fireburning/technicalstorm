package com.gionee.tutorial.reference;

import java.util.Arrays;
/**
 * 
 * @author Qingjie.Meng
 *
 */
public class SampleMain {

    public static void main(String[] arg) {
        basicTypeSample();
//        arrayTypeSample();
//        StringSample();
//        personSample();

//        StringSample2();
    }

    public static void StringSample() {
        String str = new String("Hello World!");
        changeStr(str);
        System.out.print("For String -->" + str + "\n");
    }

    public static void StringSample2() {
        String strA = "abc";
        String strB = "a" + "b";
        String strC = strB + "c";
        System.out.print((strA == strB) + "\n");
        System.out.print((strA == strC) + "\n");
    }

    public static void changeStr(String str) {
        str = "Hello Kitty!";
    }

    public static void basicTypeSample() {
        int numA = 2;
        int numB = 3;
        change(numA, numB);
        System.out.print("a = " + numA + "; b = " + numB + "\n");
    }

    public static void change(int i, int j) {
        int temp = i;
        i = j;
        j = temp;
    }

    public static void arrayTypeSample() {
        int[] intArray = { 0, 1, 2, 3, 4, 5 };
        change(intArray);
        System.out.print("intArray = " + Arrays.toString(intArray));
    }

    public static void change(int[] array) {
        array[0] = 6;
    }

    public static void personSample() {
        Person person1 = new Person();
        person1.setName("HanMeimei");
        Person person2 = new Person();
        person2.setName("Li Lei");

        changePersonName(person1, person2);
        System.out.print("person1 = " + person1 + "\n");
        System.out.print("person2 = " + person2 + "\n");

        Person person3 = null;
        initPerson(person3);
        System.out.print("person3 = " + person3 + "\n");
    }

    public static void initPerson(Person person) {
        person = new Person();
        person.setName("Tom");
        System.out.print("initPerson-->person = " + person + "\n");
    }

    public static void changePersonName(Person person1, Person person2) {
        String name1 = person1.getName();
        String name2 = person2.getName();

        person1.setName(name2);
        person2.setName(name1);
    }

    public static class Person {
        private String mName;

        public void setName(String name) {
            mName = name;
        }

        public String getName() {
            return mName;
        }

        @Override
        public String toString() {
            return "My name is " + mName;
        }

    }
}
